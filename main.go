package main

import (
	"bufio"
	"fmt"
	"os"
)

func solveMain(filename string) {
	file, err := os.Open(filename)
	if err != nil {
		print(fmt.Sprintf("file: %v cannot be used.\n", filename))
	}
	defer file.Close()

	sc := bufio.NewScanner(file)
	for i := 1; sc.Scan(); i++ {
		var (
			sudoku Sudoku
			err    error
		)

		line := sc.Text()

		if removeWhitespace(line) == "" {
			continue
		}

		sudoku, err = ParseSudoku(line)
		if err != nil {
			fmt.Printf("%4d: %v\n", i, err)
		} else {
			fmt.Printf("%4d: %s\n", i, sudoku.Oneline())
		}
	}
}

func main() {
	solveMain(`question.txt`)
}

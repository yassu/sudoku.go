package main

import (
	"fmt"
)

type position struct {
	y, x int
}

func Solve(sudoku_ Sudoku) (Sudoku, error) {
	sudoku := sudoku_.Copy()
	for {
		positions := getDecidedMap(sudoku)
		for pos, cell := range positions {
			sudoku.Mat[pos.y][pos.x] = cell
		}

		if !sudoku.Valid() {
			return Sudoku{}, fmt.Errorf("Error: %vhas no solution.", sudoku_.Oneline())
		}
		if len(positions) == 0 {
			return sudoku, nil
		}
	}
}

func getDecidedMap(sudoku Sudoku) map[position]Cell {
	decidedFs := []func(Sudoku) map[position]Cell{
		getDecidedMapHeight,
		getDecidedMapVertical,
		getDecidedMapInCell,
	}

	positions := map[position]Cell{}
	for _, f := range decidedFs {
		positionMap := f(sudoku)
		for pos, cell := range positionMap {
			positions[pos] = cell
		}
	}

	return positions
}

func getDecidedMapHeight(sudoku Sudoku) map[position]Cell {
	size := sudoku.Size()
	var positions = map[position]Cell{}

	for y := 0; y < size; y++ {
		hiddens := make([]int, 0)
		numbers := make([]int, 0)
		for x := 0; x < size; x++ {
			cell := sudoku.Mat[y][x]
			if (cell == Cell{UNKNOWN_NUMBER}) {
				hiddens = append(hiddens, x)
			} else {
				numbers = append(numbers, cell.Number)
			}
		}
		if len(numbers) == size-1 && len(hiddens) == 1 {
			positions[position{y, hiddens[0]}] = Cell{undefinedNumber(numbers)}
		}
	}

	return positions
}

func getDecidedMapVertical(sudoku Sudoku) map[position]Cell {
	size := sudoku.Size()
	var positions = map[position]Cell{}

	for x := 0; x < size; x++ {
		hiddens := make([]int, 0)
		numbers := make([]int, 0)
		for y := 0; y < size; y++ {
			cell := sudoku.Mat[y][x]
			if (cell == Cell{UNKNOWN_NUMBER}) {
				hiddens = append(hiddens, y)
			} else {
				numbers = append(numbers, cell.Number)
			}
		}
		if len(numbers) == size-1 && len(hiddens) == 1 {
			positions[position{hiddens[0], x}] = Cell{undefinedNumber(numbers)}
		}
	}

	return positions
}

func getDecidedMapInCell(sudoku Sudoku) map[position]Cell {
	size := sudoku.Size()
	sqSize := SQRT_MAP[size]
	var positions = map[position]Cell{}

	for y := 0; y < sqSize; y++ {
		for x := 0; x < sqSize; x++ {
			hiddenys := make([]int, 0)
			hiddenxs := make([]int, 0)
			numbers := make([]int, 0)
			for ry := 0; ry < sqSize; ry++ {
				for rx := 0; rx < sqSize; rx++ {
					cell := sudoku.Mat[sqSize*y+ry][sqSize*x+rx]
					if (cell == Cell{UNKNOWN_NUMBER}) {
						hiddenys = append(hiddenys, ry)
						hiddenxs = append(hiddenys, rx)
					} else {
						numbers = append(numbers, cell.Number)
					}
				}
			}
			if len(numbers) == size-1 && len(hiddenys) == 1 {
				positions[position{sqSize*y + hiddenys[0], sqSize*x + hiddenxs[0]}] = Cell{undefinedNumber(numbers)}
			}
		}
	}

	return positions
}

func containInt(xs []int, x int) bool {
	for _, v := range xs {
		if v == x {
			return true
		}
	}
	return false
}

func undefinedNumber(a []int) int {
	// 配列[1, 2, ... len(a) + 1]に含まれていて aに入っている要素を一つ返す
	indexes := make([]int, len(a)+1)
	for i := 0; i < len(indexes); i++ {
		indexes[i] = i + 1
	}

	for _, i_v := range indexes {
		defined := false
		for _, a_v := range a {
			if a_v == i_v {
				defined = true
				break
			}
		}

		if !defined {
			return i_v
		}
	}

	return -1
}

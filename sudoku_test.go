package main

import (
	"reflect"
	"strings"
	"testing"
)

func TestCell_String(t *testing.T) {
	actual := Cell{3}.String()
	expected := "3"

	if actual != expected {
		t.Errorf("actual: %v", actual)
		t.Errorf("expected: %v", expected)
	}
}

func TestCell_String2(t *testing.T) {
	actual := Cell{UNKNOWN_NUMBER}.String()
	expected := "."

	if actual != expected {
		t.Errorf("actual: %v", actual)
		t.Errorf("expected: %v", expected)
	}
}

func TestSudoku_String(t *testing.T) {
	sudoku := Sudoku{[][]Cell{
		{Cell{1}, Cell{2}, Cell{3}},
		{Cell{4}, Cell{5}, Cell{6}},
		{Cell{7}, Cell{8}, Cell{9}},
	}}
	actual := sudoku.String()
	expected := "Sudoku[\n" +
		"  123\n" +
		"  456\n" +
		"  789]"

	if actual != expected {
		t.Errorf("actual: %v", actual)
		t.Errorf("expected: %v", expected)
	}
}

func TestSudoku_Oneline(t *testing.T) {
	sudoku, _ := ParseSudoku(`
		1.3
		.4.
		59.
	`)
	actual := sudoku.Oneline()
	expected := "1.3 .4. 59. "

	if actual != expected {
		t.Errorf("actual: %v", actual)
		t.Errorf("expected: %v", expected)
	}
}

func TestSudoku_Copy(t *testing.T) {
	sudoku := Sudoku{[][]Cell{
		{Cell{1}, Cell{2}, Cell{3}},
		{Cell{4}, Cell{5}, Cell{6}},
		{Cell{7}, Cell{8}, Cell{9}},
	}}
	sudoku2 := sudoku.Copy()
	sudoku2.Mat[2][0] = Cell{100}

	if reflect.DeepEqual(sudoku, sudoku2) {
		t.Errorf("result: %v", sudoku)
	}
}

func Test_Valid(t *testing.T) {
	sudoku, _ := ParseSudoku(`
		..6 ... ...
		..4 ... ...
		..1 ... ...
		..3 ... ...
		..9 ... ...
		... ... ...
		..8 ... ...
		..5 ... ...
		..7 ... ...
	`)
	actual := sudoku.Valid()
	expected := true

	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("actual: %v", actual)
		t.Errorf("expected: %v", expected)
		t.Errorf("sudoku: %v", sudoku)
	}
}

func Test_Valid2(t *testing.T) {
	sudoku, _ := ParseSudoku(`
		... ... ...
		... ... ...
		... ... ...
		... ... 647
		... ... 839
		... ... 57.
		... ... ...
		... ... ...
		... ... ...
	`)
	actual := sudoku.Valid()
	expected := false

	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("actual: %v", actual)
		t.Errorf("expected: %v", expected)
		t.Errorf("sudoku: %v", sudoku)
	}
}

func Test_validHeight(t *testing.T) {
	sudoku, _ := ParseSudoku(`
		... ... ...
		... ... ...
		641 39. 857
		... ... ...
		... ... ...
		... ... ...
		... ... ...
		... ... ...
		... ... ...
	`)
	actual := sudoku.validHeight()
	expected := true

	if actual != expected {
		t.Errorf("actual: %v", actual)
		t.Errorf("expected: %v", expected)
		t.Errorf("sudoku: %v", sudoku)
	}
}

func Test_validHeight2(t *testing.T) {
	sudoku, _ := ParseSudoku(`
		... ... ...
		... ... ...
		641 39. 757
		... ... ...
		... ... ...
		... ... ...
		... ... ...
		... ... ...
		... ... ...
	`)
	actual := sudoku.validHeight()
	expected := false

	if actual != expected {
		t.Errorf("actual: %v", actual)
		t.Errorf("expected: %v", expected)
		t.Errorf("sudoku: %v", sudoku)
	}
}

func Test_validVertical(t *testing.T) {
	sudoku, _ := ParseSudoku(`
		..6 ... ...
		..4 ... ...
		..1 ... ...
		..3 ... ...
		..9 ... ...
		... ... ...
		..8 ... ...
		..5 ... ...
		..7 ... ...
	`)
	actual := sudoku.validVertical()
	expected := true

	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("actual: %v", actual)
		t.Errorf("expected: %v", expected)
		t.Errorf("sudoku: %v", sudoku)
	}
}

func Test_validVertical2(t *testing.T) {
	sudoku, _ := ParseSudoku(`
		..6 ... ...
		..4 ... ...
		..8 ... ...
		..3 ... ...
		..9 ... ...
		... ... ...
		..8 ... ...
		..5 ... ...
		..7 ... ...
	`)
	actual := sudoku.validVertical()
	expected := false

	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("actual: %v", actual)
		t.Errorf("expected: %v", expected)
		t.Errorf("sudoku: %v", sudoku)
	}
}

func Test_validInCell(t *testing.T) {
	sudoku, _ := ParseSudoku(`
		... ... ...
		... ... ...
		... ... ...
		... ... 641
		... ... 839
		... ... 57.
		... ... ...
		... ... ...
		... ... ...
	`)
	actual := sudoku.validInCell()
	expected := true

	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("actual: %v", actual)
		t.Errorf("expected: %v", expected)
		t.Errorf("sudoku: %v", sudoku)
	}
}

func Test_validInCell2(t *testing.T) {
	sudoku, _ := ParseSudoku(`
		... ... ...
		... ... ...
		... ... ...
		... ... 647
		... ... 839
		... ... 57.
		... ... ...
		... ... ...
		... ... ...
	`)
	actual := sudoku.validInCell()
	expected := false

	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("actual: %v", actual)
		t.Errorf("expected: %v", expected)
		t.Errorf("sudoku: %v", sudoku)
	}
}

func Test_ParseSudoku(t *testing.T) {
	_, err := ParseSudoku(`
	123
	456
	789
	`)

	if err != nil {
		t.Errorf("error: %v", err)
	}
}

func Test_ParseSudoku2(t *testing.T) {
	_, err := ParseSudoku(`
	12.
	456
	789
	`)

	if err != nil {
		t.Errorf("error: %v", err)
	}
}

func Test_ParseSudoku3(t *testing.T) {
	_, err := ParseSudoku(`
	123
	456
	`)
	if strings.Index(err.Error(), "is not a square number") == -1 {
		t.Errorf("error: %v", err)
	}
}

func Test_ParseSudoku4(t *testing.T) {
	_, err := ParseSudoku(`
	123
	456
	abc
	`)
	if strings.Index(err.Error(), "converted to") == -1 {
		t.Errorf("error: %v", err)
	}
}

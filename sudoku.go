package main

import (
	"fmt"
	"strconv"
	"strings"
)

const UNKNOWN_NUMBER = -1
const UNKNOWN_STRING = "."

var SQRT_MAP = map[int]int{
	1:   1,
	4:   2,
	9:   3,
	16:  4,
	25:  5,
	36:  6,
	49:  7,
	64:  8,
	81:  9,
	100: 10,
}

type Cell struct {
	Number int
}

func (cell Cell) String() string {
	if cell.Number == UNKNOWN_NUMBER {
		return UNKNOWN_STRING
	} else {
		return strconv.Itoa(cell.Number)
	}
}

type Sudoku struct {
	Mat [][]Cell
}

func (sudoku Sudoku) Size() int {
	return len(sudoku.Mat)
}

func (sudoku Sudoku) Copy() Sudoku {
	size := sudoku.Size()
	dest := make([][]Cell, size)
	for i, row := range sudoku.Mat {
		dest[i] = make([]Cell, size)
		copy(dest[i], row)
	}
	return Sudoku{dest}
}

func (sudoku Sudoku) Valid() bool {
	return sudoku.validHeight() && sudoku.validVertical() && sudoku.validInCell()
}

func (sudoku Sudoku) validHeight() bool {
	size := sudoku.Size()

	for y := 0; y < size; y++ {
		cells := make([]Cell, 0)
		for x := 0; x < size; x++ {
			cell := sudoku.Mat[y][x]
			if (cell != Cell{UNKNOWN_NUMBER}) {
				if containCell(cells, cell) {
					return false
				}
				cells = append(cells, cell)
			}
		}
	}

	return true
}

func (sudoku Sudoku) validVertical() bool {
	size := sudoku.Size()

	for x := 0; x < size; x++ {
		cells := make([]Cell, 0)
		for y := 0; y < size; y++ {
			cell := sudoku.Mat[y][x]
			if (cell != Cell{UNKNOWN_NUMBER}) {
				if containCell(cells, cell) {
					return false
				}
				cells = append(cells, cell)
			}
		}
	}

	return true
}

func (sudoku Sudoku) validInCell() bool {
	size := sudoku.Size()
	sqSize := SQRT_MAP[size]

	for y := 0; y < sqSize; y++ {
		for x := 0; x < sqSize; x++ {
			cells := make([]Cell, 0)
			for ry := 0; ry < sqSize; ry++ {
				for rx := 0; rx < sqSize; rx++ {
					cell := sudoku.Mat[sqSize*y+ry][sqSize*x+rx]
					if (cell != Cell{UNKNOWN_NUMBER}) {
						if containCell(cells, cell) {
							return false
						}
						cells = append(cells, cell)
					}
				}
			}
		}
	}

	return true
}

func (sudoku Sudoku) String() string {
	s := "Sudoku["
	for _, row := range sudoku.Mat {
		s += "\n  "
		for _, v := range row {
			s += fmt.Sprintf("%v", v)
		}
	}
	s += "]"

	return s
}

func (sudoku Sudoku) Oneline() string {
	s := ""

	for _, row := range sudoku.Mat {
		for _, v := range row {
			s += fmt.Sprintf("%v", v)
		}
		s += " "
	}

	return s
}

func ParseSudoku(s string) (Sudoku, error) {
	sudoku_s := removeWhitespace(s)

	size, err := SQRT_MAP[len(sudoku_s)]

	if !err {
		return Sudoku{}, fmt.Errorf("Error: %d is not a square number.", len(sudoku_s))
	}

	mat := make([][]Cell, size)
	for i := 0; i < size; i++ {
		mat[i] = make([]Cell, size)
	}

	for i, v := range sudoku_s {
		var cell Cell

		if string(v) == UNKNOWN_STRING {
			cell = Cell{UNKNOWN_NUMBER}
		} else {
			n, err := strconv.Atoi(string(v))
			if err != nil {
				return Sudoku{}, fmt.Errorf("Error: %s cannot to be converted to sudoku cell.", string(v))
			}

			cell = Cell{n}
		}

		mat[i/size][i%size] = cell
	}

	return Sudoku{mat}, nil
}

func removeWhitespace(s string) string {
	whitespace := []string{" ", "\t", "\n", "\r", "\x0b", "\x0c"}
	res := s
	for _, v := range whitespace {
		res = strings.Replace(res, v, "", -1)
	}

	return res
}

func containCell(xs []Cell, x Cell) bool {
	for _, v := range xs {
		if v == x {
			return true
		}
	}
	return false
}
